<?php
    //Debug information
    $debug = 0;

    /* [VERIFY CAPTCHA FIRST] */
    $secret = '6Le2aawUAAAAAEYXegkieXZG5AlWvXQu1Bi1a_aL';
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$_POST['g-recaptcha-response'];
    $verify = json_decode(file_get_contents($url));

    if ($verify->success) {

        //get the parameters
        $name = $_POST["name"];
        $email = $_POST["email"];
        $message = $_POST["message"];

        $emailMessage = "<br>Name = " . $name;
        $emailMessage .= "<br>Email = " . $email;
        $emailMessage .= "<br>Message = " . $message;

        if ($debug == 1) {
            echo "<br>Message = " . $emailMessage;
        }

        include 'mail/sendMail.php';

        //Mandando Email
        $from = "info@vazab.com";
        $to_customer = "hvabrego@hotmail.com";
        $subject = "Deptos - Solicitud de Informacion";
        $email_text = $emailMessage;


        if ($debug == 1) {
            echo "<br>Enviando Correo<br>";
        }
        $resultMail = sendMail($from, $to_customer, "", $subject, $email_text);

        if ($debug == 1) {
            echo "Email = " . $resultMail;
            echo "<br><br>";
        }
    }
?>

<!DOCTYPE HTML>

<html>
<head>
    <title>Departamentos Amueblados en Saltillo Coahuila Mexico para Estancias Temporales</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="Departamentos Amueblados en Saltillo Coahuila Mexico para Estancias Temporales" />
    <meta name="keywords" content="Departamentos, Amueblados, Saltillo, Coahuila, Mexico, Estancias, Temporales, Inmobiliaria, Rentas" />
    <meta property="og:title" content="Departamentos Amueblados en Saltillo Coahuila Mexico" />
    <meta property="og:description" content="Departamentos Amueblados en Saltillo Coahuila Mexico para Estancias Temporales" />
    <meta property="og:url" content="http://www.departamentosamuebladosensaltillo.com/" />
    <link href='http://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
    <!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-panels.min.js"></script>
    <script src="js/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="css/skel-noscript.css" />
        <link rel="stylesheet" href="css/style.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
</head>
<body class="homepage">

<!-- Header -->
<div id="header">
    <div class="container">

        <!-- Logo -->
        <div id="logo">
            <!--h1><a href="#" >Departamentos Amueblados</a></h1>
            <span>en Saltillo, Coahuila</span-->
        </div>

        <!-- Nav -->
        <nav id="nav">
            <ul>
                <li class="active"><a href="index.html">Inicio</a></li>
                <li><a href="depto208a.html">Departamento A</a></li>
                <li><a href="depto208b.html">Departamento B</a></li>
                <li><a href="depto208c.html">Departamento C</a></li>
            </ul>
        </nav>

    </div>
</div>
<!-- Header -->

<!-- Main -->
<div id="main">
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div id="content" class="12u skel-cell-important">
                <section>
                    <header>
                        <h2 align="center">Gracias por tu mensaje, <br><br> nos pondremos en contacto a la brevedad.</h2>
                    </header>

                </section>
            </div>
            <!-- /Content -->

        </div>

    </div>
</div>
<!-- Main -->


<!-- Copyright -->
<div id="copyright">
    <div class="container">
        &copy; Vazab 2018
    </div>
</div>

</body>
</html>
